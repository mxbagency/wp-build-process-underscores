// INSTRUCTIONS //
// Change variables at the top of this file
// Run "gulp build"

// CHANGE THESE VARS //
var themeName = 'Test MXB';
var themeSlug = 'TestMXB';
var themeShort = 'testmxb';
var themeTech = 'testmxb';

// REQUIRE GULP PLUGINS
var gulp = require('gulp');
var ggit = require('gulp-git');
var clean = require('gulp-clean');
var replace = require('gulp-replace');
var download = require('gulp-download');
var unzip = require('gulp-unzip');
var fs = require('fs');

// DELETES THE TMP FOLDER
gulp.task('clean-files', function(done) {
  gulp.src(['tmp', 'wordpress'], {read: false, allowEmpty: true})
  .pipe(clean());
  done();
});

// GET LATEST WORDPRESS
gulp.task('wp-clone', function(){
  return download('https://wordpress.org/latest.zip')
  .pipe(unzip())
  .pipe(gulp.dest('.'));
});

// RENAMES WORDPRESS TO THEME TECH NAME
gulp.task('rename-wordpress', function(done) {
  fs.rename('wordpress', themeTech, function (err) {
    if (err) {
      throw err;
    }
  });
  done();
});

// DELETES UNNECESSARY WORDPRESS PLUGINS
gulp.task('del-default-plugins', function () {
  return gulp.src([themeTech + '/wp-content/plugins/hello.php', themeTech + '/wp-content/plugins/akismet/'], {read: false, allowEmpty:true})
  .pipe(clean());
});

// DELETES UNNECESSARY WORDPRESS THEMES
gulp.task('del-default-themes', function () {
  return gulp.src(themeTech + '/wp-content/themes/', {read: false, allowEmpty:true})
  .pipe(clean());
});

// MXB STARTER THEME CLONE
gulp.task('theme-clone', function(done){
  ggit.clone('https://bitbucket.org/mxbagency/mxb-starter-theme-underscores/get/master.zip', {args: themeTech + '/wp-content/themes/' + themeTech }, function (err) {
    if (err) throw err;
  });
  done();
});

// FREQUENT DEPENDENCIES LIKE CMB2, SLICK ETC.
gulp.task('deps-tmp', function() {
	return download([
    'https://github.com/wp-plugins/all-in-one-wp-security-and-firewall/archive/master.zip',
    'http://downloads.wordpress.org/plugin/w3-total-cache.zip',
	  'https://github.com/kenwheeler/slick/archive/master.zip'])
	  .pipe(unzip())
    .pipe(gulp.dest('tmp'));
});

// CLONE LOCAL DEVELOPMENT GULPFILE AND PACKAGE.JSON
gulp.task('dev-deps-clone', function(done){
  ggit.clone('https://bitbucket.org/mxbagency/local-dev-files-underscores/get/master.zip', {args: 'tmp/localdev' }, function (err) {
    if (err) throw err;
  });
  done();
});


// MOVE DEPS INTO THEME FOLDERS
gulp.task('deps-move', function(done) {

  gulp.src('tmp/slick-master/slick/**/*', { base: 'tmp/slick-master/slick/' })
    .pipe(gulp.dest(themeTech + '/wp-content/themes/' + themeTech + '/inc/slick' ))

  gulp.src('tmp/all-in-one-wp-security-and-firewall-master/**/*', { base: 'tmp/all-in-one-wp-security-and-firewall-master/' })
    .pipe(gulp.dest(themeTech + '/wp-content/plugins/all-in-one-wp-security-and-firewall' ))
  
  gulp.src('tmp/w3-total-cache/**/*', { base: 'tmp/w3-total-cache/' })
    .pipe(gulp.dest(themeTech + '/wp-content/plugins/w3-total-cache' ))

  gulp.src(['tmp/localdev/gulpfile.js', 'tmp/localdev/package.json'], { base: 'tmp/localdev' })
    .pipe(gulp.dest(themeTech));

  gulp.src(['tmp/localdev/.eslintrc.json', 'tmp/localdev/.eslintrc.json'], { base: 'tmp/localdev' })
    .pipe(gulp.dest(themeTech));

  done();
});

// REPLACES ALL THE "REPLACE ME" TEXT
gulp.task('theme-clean', function() {
   gulp.src(themeTech + '/wp-content/themes/' + themeTech + '/style.css')
    .pipe(replace('Theme Name: Replace Me', 'Theme Name: ' + themeName))
    .pipe(gulp.dest(themeTech + '/wp-content/themes/' + themeTech + '/'))
    .pipe(replace('Text Domain: replace-me', 'Text Domain: ' + themeSlug))
    .pipe(gulp.dest(themeTech + '/wp-content/themes/' + themeTech + '/'))

  return gulp.src(themeTech + '/wp-content/themes/' + themeTech + '/**/*')

    .pipe(replace('replace-me_', themeShort + '_'))
    .pipe(gulp.dest(themeTech + '/wp-content/themes/' + themeTech + '/'))
    
    .pipe(replace('rm_', themeShort + '_'))
    .pipe(gulp.dest(themeTech + '/wp-content/themes/' + themeTech + '/'))
    
    .pipe(replace(' replace-me', ' ' + themeShort))
    .pipe(gulp.dest(themeTech + '/wp-content/themes/' + themeTech + '/'))
    
    .pipe(replace('replace-me-', themeShort + '-'))
    .pipe(gulp.dest(themeTech + '/wp-content/themes/' + themeTech + '/'))
    
    .pipe(replace('replace-me', themeShort))
    .pipe(gulp.dest(themeTech + '/wp-content/themes/' + themeTech + '/'))
    
    .pipe(replace('replace_me', themeShort))
    .pipe(gulp.dest(themeTech + '/wp-content/themes/' + themeTech + '/'))
    
    .pipe(replace('Replace Me', themeName))
    .pipe(gulp.dest(themeTech + '/wp-content/themes/' + themeTech + '/'))
    
    .pipe(replace('Replace_Me', themeTech))
    .pipe(gulp.dest(themeTech + '/wp-content/themes/' + themeTech + '/'));
});

// REPLACES THE THEME NAME IN THE GULP FILE
gulp.task('rename-gulpfile', function(done) {
  gulp.src(themeTech + '/gulpfile.js')
    .pipe(replace('replace-me', themeTech))
    .pipe(gulp.dest(themeTech));
    done();
})

// DELETES THE TMP FOLDER & GIT FILES
gulp.task('clean-git', function() {
  return gulp.src([themeTech + '/**/.git', 'local-dev-files/**/.git'], {read: false, allowEmpty: false})
  .pipe(clean());
});

// MAIN BUILD TASK
gulp.task('build', gulp.series(
  'clean-files',
  'wp-clone',
  'rename-wordpress',
  'del-default-plugins',
  'del-default-themes',
  'theme-clone',
  'dev-deps-clone',
  'deps-tmp',
  'deps-move',
  'theme-clean',
  'rename-gulpfile',
  'clean-git'
));